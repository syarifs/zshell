export WORDCHARS='*?_-.[]~=&;!#%^(){}<>'

# === ALIAS ===
alias gi='git init'
alias gra='git remote add'
alias gru='gitremoteurl'
alias ga='git add'
alias gaa='git add .'
alias gcm='git commit -m'
alias gp='git push'
alias v='nvim'
alias install='sudo pacman -S'
alias upgrade='sudo pacman -Syyu'
alias remove='sudo pacman -Rsnc'

